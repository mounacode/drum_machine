import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Pad from './Pad.jsx';
import { useState } from 'react';
const audioClips=[
  {
    keyCode:81,
    keyTriger:'Q',
    id:"Heater-1",
    url:"https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3",
  },
  {
    keyCode:87,
    keyTriger:'W',
    id:"Heater-é",
    url:"https://s3.amazonaws.com/freecodecamp/drums/Heater-2.mp3",
  },
  {
    keyCode:69,
    keyTriger:'E',
    id:"Heater-3",
    url:"https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3",
  },
  {
    keyCode:65,
    keyTriger:'A',
    id:"Heater-4",
    url:"https://s3.amazonaws.com/freecodecamp/drums/Heater-4_1.mp3",
  },
  {
    keyCode:83,
    keyTriger:'S',
    id:"Clap",
    url:"https://s3.amazonaws.com/freecodecamp/drums/Heater-6.mp3",
  },
  {
    keyCode:68,
    keyTriger:'D',
    id:"Open-HH",
    url:"https://s3.amazonaws.com/freecodecamp/drums/Dsc_Oh.mp3",
  },
  {
    keyCode:90,
    keyTriger:'Z',
    id:"Kick-n'-Hat",
    url:"https://s3.amazonaws.com/freecodecamp/drums/Kick_n_Hat.mp3",
  },
  {
    keyCode:88,
    keyTriger:'X',
    id:"Kick",
    url:"https://s3.amazonaws.com/freecodecamp/drums/RP4_KICK_1.mp3",
  },
  {
    keyCode:67,
    keyTriger:'C',
    id:"Closed-HH",
    url:"https://s3.amazonaws.com/freecodecamp/drums/Cev_H2.mp3",
  },

]

function App() {
  const [volume,setVolume]= useState(1)
  const [record, setRecord]= useState("")
  const [speed,setSpeed] = useState(0.5)

  const playRecord= ()=>{
     let index = 0;
     let recordArray = record.split(' ')   
    const interval = setInterval(()=>{
      const audioTag= document.getElementById(recordArray[index])
    
      audioTag.volume= volume;
      audioTag.currentTime=0;
      audioTag.play();
      index += 1;

     },speed * 600);
     setTimeout(()=> clearInterval(interval), 600 * speed * recordArray.length - 1)
  }
  return (
    <div className="bg-info min-vh-100 text-white">
      <div className="text-center">
         <h2 className="drum-machine">
          Drum Machine
         </h2>
        {
          audioClips.map((clip) => (
          
            <Pad key={clip.id} clip = {clip} volume={volume} setRecord={setRecord}/>
           ))
        }
        <br />
        <h4 className="">Volume</h4>
        <input type="range" step="0.01" max="1" min="0" value={volume} className='w-50'
        onChange={(e)=> setVolume(e.target.value)} />
        <h3>{record}</h3>
          {
            record && (<>
                     <button onClick={()=>playRecord()} className="btn btn-success">Play</button>
                     <button  onClick={(e)=>setRecord('')}  className="btn btn-danger me-3">Clear</button>
                     <br />
                     <h4>Speed</h4>
                     <input type="range" step="0.01" max="1.2" min="0.1" value={speed} className='w-50'
        onChange={(e)=> setSpeed(e.target.value)} />
            </>)
          }

       </div>

    </div>
  );
}

export default App;
