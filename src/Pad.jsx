import { useEffect, useState } from "react";


const Pad = ({clip,volume,setRecord}) => {
const [active,setActive]= useState(false)

 useEffect(()=>{
      document.addEventListener('keydown', handleKeyPress);
      return ()=>{
        document.removeEventListener('keydown', handleKeyPress);
      }
 },[clip]);
 const handleKeyPress= (e)=>{
    if(e.keyCode===clip.keyCode){
        playSound()
    }
}

    const playSound= ()=>{
        const audioTag= document.getElementById(clip.keyTriger);
        
        setActive(true);
        setTimeout(()=> setActive(false),200)
        audioTag.volume= volume;
        audioTag.currentTime=0;
        audioTag.play();
        setRecord((prev)=>prev + clip.keyTriger + " ")
    }
   
  return (
    <div className="btn btn-secondary p-4 m-3" onClick={playSound}>
       <audio src={clip.url} className='clip' id={clip.keyTriger}/>
       {clip.keyTriger} 
    </div>
  )
}

export default Pad
